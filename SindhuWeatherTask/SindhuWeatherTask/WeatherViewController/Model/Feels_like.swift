//
//  Feels_like.swift
//  SindhuWeatherTask
//
//  Created by Sindhu Thota on 03/24/23.
//

import Foundation
struct Feels_like : Codable {
	let day : Double?
	let night : Double?
	let eve : Double?
	let morn : Double?
}
