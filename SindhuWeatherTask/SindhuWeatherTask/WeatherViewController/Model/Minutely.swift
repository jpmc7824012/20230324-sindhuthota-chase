//
//  Minutely.swift
//  SindhuWeatherTask
//
//  Created by Sindhu Thota on 03/24/23.
//

import Foundation
struct Minutely : Codable {
	let dt : Int?
	let precipitation : Int?
}
