//
//  Temp.swift
//  SindhuWeatherTask
//
//  Created by Sindhu Thota on 03/24/23.
//

import Foundation
struct Temp : Codable {
	let day : Double?
	let min : Double?
	let max : Double?
	let night : Double?
	let eve : Double?
	let morn : Double?
}
