//
//  SindhuWeatherTaskTests.swift
//  SindhuWeatherTaskTests
//
//  Created by Sindhu Thota on 10/03/23.
//

import XCTest
@testable import SindhuWeatherTask

final class SindhuWeatherTaskTests: XCTestCase {

    var vc: WeatherDetailsViewController!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func setUp() {
        vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WeatherDetailsViewController") as? WeatherDetailsViewController
               _ = vc.view
        let mockApiManager = MockAPIManager.sharedManager
        vc.viewModel = WeatherDetailsViewModel(apimanager: mockApiManager)
    }
   
    func testCityWeatherDetails() {
        XCTAssertNotNil(self.vc.viewModel.apiManager)
        vc.viewModel.getCityWeatherDetails(cityName: "texas") { status, error in
            XCTAssertTrue(status)
            XCTAssertNotNil(self.vc.viewModel.coordinate)
        }
    }
    
    func testCoodinatesWeatherDetails() {
        let coodinates = Coord(lon: 25.00, lat: 36.00)
        vc.viewModel.updateCoordinateLocation(location: coodinates)
        XCTAssertNotNil(self.vc.viewModel.coordinate)
        vc.viewModel.getWeatherResults { status, error in
            XCTAssertTrue(status)
            XCTAssertNotNil(self.vc.viewModel.weatherData)
        }
        vc.getWeatherData()
        XCTAssertNotNil(self.vc.viewModel.weatherData)
        vc.searchTextfield.text = "Frisco"
        vc.getCityDetails(cityName: "Frisco")
        XCTAssertEqual(self.vc.viewModel.coordinate?.lat, 40.1671)
    }

    func testTableViewDelegates(){
      let numOfRows = vc.tableView(vc.weatherTableView, numberOfRowsInSection: 0)
      XCTAssert(numOfRows == vc.viewModel.weatherData?.daily?.count ?? 0)
      let cell =  vc.tableView(vc.weatherTableView, cellForRowAt: IndexPath(row: 0, section: 0))
      XCTAssertNotNil(cell)
      XCTAssertNotNil(vc.weatherTableView.delegate)
      XCTAssertNotNil(vc.weatherTableView.dataSource)
    }
    
    func testTextFieldValidations() {
      vc.searchTextfield.text = "Frisco"
        vc.textFieldDidEndEditing(vc.searchTextfield)
        XCTAssertNotNil(vc.viewModel.weatherData)
        vc.searchTextfield.text = ""
        vc.textFieldDidEndEditing(vc.searchTextfield)
        XCTAssertNil(vc.viewModel.weatherData)
        XCTAssertTrue(vc.textFieldShouldReturn(vc.searchTextfield))
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
